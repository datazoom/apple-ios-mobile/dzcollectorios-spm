// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "DZ_Collector_iOS",
    platforms: [.iOS(.v13)],
    products: [
        .library(name: "DZ_Collector_iOS", targets: ["DZ_Collector_iOS"]),
    ],
    dependencies: [],
    targets: [
      .binaryTarget(name: "DZ_Collector_iOS", path: "Sources/DZ_Collector_iOS.xcframework")
    ]
)
