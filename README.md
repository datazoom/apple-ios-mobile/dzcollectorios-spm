# DZ_Collector_iOS

DataZoom iOS Collector

### Initial setup
* Download DZ_Collector_iOS.xcframework file from this repository.
* Drag and drop the downloaded frameworks into your Xcode project. Drag the file to the files inspector on the left side of Xcode. Make sure you check the box "Copy items" in the popup menu that displays while drag and drop the file.
* In selected Xcode target, go to the General tab and scroll down to Frameworks, Libraries and Embedded Content. If not present, add the framework by clicking the "+" button. Make sure that Embed option is set to Embed & Sign.

## Steps for Swift Language based Applications
After including the framework file, open the ViewController/View file, where the AVPlayer(native player) is included.

Import the framework using the following command:

```
import DZ_Collector_iOS
```

Initialize the framework by passing along the 'Configuration ID', and URL given by Datazoom. Completion handler will return two values, success and error if happened.

```
let collector = DZ_Collector_iOS(configID: <configuration id from Datazoom>, url: <url given by Datazoom>, completion: @escaping (Bool, Error?) -> ())
```

You can start recording events for your AVPlayer instance using this API call.

```collector.startRecordingEvents(for: player)```

You can also stop recording events of your AVPlayer instance using similiar API call.

```collector.stopRecordingEvents(for: player)```
Run the app and observe the events captured in a Collector, data corresponding to MOBILE/iPhone in Platform, refers to the events tracked from iPhone.

## Custom Events & Metadata
Datazoom allows customers to collect custom events and metadata that aren't emitted from the video player.

### Custom Metadata
Create an NSDictionary with necessary metadata

```
let metaData = ["customMetadata":"MetadataValue"]
```

Add the metadata to DZEventCollector

```
collector.addCustom(metadata: metadata)
```

Example:

```
func sendCustomMetadata(){
	collector.addCustomMetadata(metadata: ["customPlayerName": "iOS Native Player"])
}
```


### Custom Events
Send  Events like below:

```
collector.addCustomEvent(event: "custom_event_name", metadata: metadata|nil)
```


Example:

```
@objc func buttonPushTouched() {
  collector.customEvents("buttonClick", metadata:["customPlayerName":"NativeiOSPlayer","customDomainName":"datazoom.io"])
}
```

## Steps for ObjectiveC Language based Applications:

* After including the framework file, Create a bridging header file, to allow interoperability of languages.

# open the ViewController/View file, where the AVPlayer(native player) is included.

Import the following:
```
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import <DZ_Collector_iOS/DZ_Collector_iOS.h>
```

Initialize the swift class in the .h file.

```
DZ_Collector_iOS *collector;
```
In the .m file, allocate using:

```
collector = [[DZ_Collector_iOS alloc]initWithConfigId: <configuration id from Datazoom>, url: <>];
[collector startWithCompletion: ^(BOOL result, NSError *error) {
    ...
}];
```

Run the app and observe the events captured in a Collector, data corresponding to MOBILE/iPhone in Platform, refers to the events tracked from iPhone.

Demo Application
A demo application that shows the usage of this framework is available Here.  This can be used to test the DZ_Collector_iOS framework.
